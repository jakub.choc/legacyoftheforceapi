package com.game.LOTF.entity;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.OneToOne;
import lombok.Getter;
import lombok.Setter;

@Entity(name = "item_shop")
@Setter
@Getter
public class Vendor {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private int money;
    @OneToOne
    @JoinColumn(name = "inventory_id")
    private Inventory inventory;

    public void addMoney(int add) {
        setMoney(money + add);
    }

    public void takeMoney(int take) {
        setMoney(money - take);
    }
}
