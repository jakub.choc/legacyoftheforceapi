package com.game.LOTF.controller;

import lombok.AllArgsConstructor;
import com.game.LOTF.dto.TravelRecordDTO;
import com.game.LOTF.service.TravelService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@AllArgsConstructor
@RequestMapping("/api")
public class TravelController {

    private final TravelService travelService;

    @PostMapping("/travel")
    public TravelRecordDTO sellItem(@RequestParam Long characterId, @RequestParam Long startDestinationId, @RequestParam Long finishDestinationId) {
        return travelService.travel(characterId, startDestinationId, finishDestinationId);
    }
}
