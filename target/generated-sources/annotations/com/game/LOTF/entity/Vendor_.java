package com.game.LOTF.entity;

import jakarta.persistence.metamodel.SingularAttribute;
import jakarta.persistence.metamodel.StaticMetamodel;
import javax.annotation.processing.Generated;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Vendor.class)
public abstract class Vendor_ {

	public static volatile SingularAttribute<Vendor, Integer> money;
	public static volatile SingularAttribute<Vendor, Long> id;
	public static volatile SingularAttribute<Vendor, Inventory> inventory;

	public static final String MONEY = "money";
	public static final String ID = "id";
	public static final String INVENTORY = "inventory";

}

