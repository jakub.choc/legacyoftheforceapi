package com.game.LOTF.dto.mapper;

import com.game.LOTF.dto.FightRecapDTO;
import com.game.LOTF.entity.Character;
import com.game.LOTF.entity.FightRecap;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    comments = "version: 1.5.3.Final, compiler: javac, environment: Java 17.0.7 (Amazon.com Inc.)"
)
@Component
public class FightRecapMapperImpl implements FightRecapMapper {

    @Override
    public FightRecapDTO toDto(FightRecap fightRecap) {
        if ( fightRecap == null ) {
            return null;
        }

        FightRecapDTO fightRecapDTO = new FightRecapDTO();

        fightRecapDTO.setWinner( fightRecapLoserName( fightRecap ) );
        fightRecapDTO.setLoser( fightRecapWinnerName( fightRecap ) );
        fightRecapDTO.setId( fightRecap.getId() );
        fightRecapDTO.setFightTime( fightRecap.getFightTime() );

        return fightRecapDTO;
    }

    private String fightRecapLoserName(FightRecap fightRecap) {
        if ( fightRecap == null ) {
            return null;
        }
        Character loser = fightRecap.getLoser();
        if ( loser == null ) {
            return null;
        }
        String name = loser.getName();
        if ( name == null ) {
            return null;
        }
        return name;
    }

    private String fightRecapWinnerName(FightRecap fightRecap) {
        if ( fightRecap == null ) {
            return null;
        }
        Character winner = fightRecap.getWinner();
        if ( winner == null ) {
            return null;
        }
        String name = winner.getName();
        if ( name == null ) {
            return null;
        }
        return name;
    }
}
