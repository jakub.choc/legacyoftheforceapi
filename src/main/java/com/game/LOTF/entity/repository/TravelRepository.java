package com.game.LOTF.entity.repository;

import java.util.List;
import com.game.LOTF.entity.TravelRecord;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface TravelRepository extends JpaRepository<TravelRecord, Long> {

    @Query(
            value = """
            SELECT * FROM travel_record WHERE status = 'ACTIVE'
            """, nativeQuery = true
    )
    List<TravelRecord> getAllActiveTravels();
}
