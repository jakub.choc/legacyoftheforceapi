DROP DATABASE IF EXISTS gamedatabase;
CREATE DATABASE gamedatabase;
USE gamedatabase;
SET FOREIGN_KEY_CHECKS=0;

CREATE TABLE if not exists inventory
(
    id bigint NOT NULL AUTO_INCREMENT,
    character_id bigint DEFAULT NULL,
    PRIMARY KEY (`id`),
    UNIQUE KEY `UK_3gtrq7nvmaib22jhnaecnxwqx` (`character_id`),
    CONSTRAINT `FKo1ybqr8ec2slqbrj3pfl96w4w` FOREIGN KEY (`character_id`) REFERENCES `warrior` (`id`)
);

CREATE TABLE if not exists item
(
    id bigint NOT NULL AUTO_INCREMENT,
    icon varchar(255) DEFAULT NULL,
    name varchar(255) DEFAULT NULL,
    inventory_id bigint NOT NULL,
    item_id bigint NOT NULL,
    buy_price int NOT NULL,
    PRIMARY KEY (`id`),
    KEY `FK4ge8oeo21psdvmd7nlcgm3eoc` (`item_id`),
    KEY `FK69jrwlwin8x1qcbmg3k8ut7w1` (`inventory_id`),
    CONSTRAINT `FK4ge8oeo21psdvmd7nlcgm3eoc` FOREIGN KEY (`item_id`) REFERENCES `item` (`id`),
    CONSTRAINT `FK69jrwlwin8x1qcbmg3k8ut7w1` FOREIGN KEY (`inventory_id`) REFERENCES `inventory` (`id`)
);

CREATE TABLE if not exists item_inventory
(
    item_id bigint NOT NULL,
    inventory_id bigint NOT NULL,
    KEY `FKc8wb0ndr6m9sevqwl8afcxam8` (`inventory_id`),
    KEY `FKis34mg9pc892kd0968g1rb9pt` (`item_id`),
    CONSTRAINT `FKc8wb0ndr6m9sevqwl8afcxam8` FOREIGN KEY (`inventory_id`) REFERENCES `inventory` (`id`),
    CONSTRAINT `FKis34mg9pc892kd0968g1rb9pt` FOREIGN KEY (`item_id`) REFERENCES `item` (`id`)
);

CREATE TABLE if not exists race
(
    id          bigint NOT NULL AUTO_INCREMENT,
    race_name   varchar(255) DEFAULT NULL,
    PRIMARY KEY (`id`)
);

CREATE TABLE if not exists warrior
(
    id              bigint          NOT NULL AUTO_INCREMENT,
    name            varchar(255)    DEFAULT NULL,
    profile_photo   varchar(255)    DEFAULT NULL,
    race_id         bigint          DEFAULT NULL,
    inventory_id    bigint          DEFAULT NULL,
    armour          int             NOT NULL,
    damage          int             NOT NULL,
    experience      int             NOT NULL,
    health          int             NOT NULL,
    max_experience  int             NOT NULL,
    max_health      int             NOT NULL,
    max_stamina     int             NOT NULL,
    stamina         int             NOT NULL,
    level           int             NOT NULL,
    money           int             NOT NULL,
    score           int             NOT NULL,
    PRIMARY KEY (`id`),
    KEY `FK29i6yv3em5r3569sydhq5ycbq` (`race_id`),
    KEY `FK8i9905qd1jieb6nrehffbf83u` (`inventory_id`),
    CONSTRAINT `FK29i6yv3em5r3569sydhq5ycbq` FOREIGN KEY (`race_id`) REFERENCES `race` (`id`),
    CONSTRAINT `FK8i9905qd1jieb6nrehffbf83u` FOREIGN KEY (`inventory_id`) REFERENCES `inventory` (`id`)
);


CREATE TABLE if not exists item_shop
(
                             id bigint NOT NULL AUTO_INCREMENT,
                             money int NOT NULL,
                             inventory_id bigint DEFAULT NULL,
                             PRIMARY KEY (`id`),
                             UNIQUE KEY `UK_hapb6dytv2o42apnfsxja2t0d` (`inventory_id`),
                             CONSTRAINT `FKnx5ugp55e7aow44bh0pfyt6sj` FOREIGN KEY (`inventory_id`) REFERENCES `inventory` (`id`)
);


CREATE TABLE if not exists inventory_item
(
                                  inventory_id bigint NOT NULL,
                                  item_id bigint NOT NULL,
                                  KEY `FKd1g8wrnylpjywj6l32f0wukig` (`item_id`),
                                  KEY `FKk5dyq25q0q8qyxr17iw854bqm` (`inventory_id`),
                                  CONSTRAINT `FKd1g8wrnylpjywj6l32f0wukig` FOREIGN KEY (`item_id`) REFERENCES `item` (`id`),
                                  CONSTRAINT `FKk5dyq25q0q8qyxr17iw854bqm` FOREIGN KEY (`inventory_id`) REFERENCES `inventory` (`id`)
);

CREATE TABLE if not exists fight_recap
(
                               id bigint NOT NULL AUTO_INCREMENT,
                               fight_time datetime(6) DEFAULT NULL,
                               loser_id bigint DEFAULT NULL,
                               winner_id bigint DEFAULT NULL,
                               PRIMARY KEY (`id`),
                               KEY `FKrd50k8m9ppvm01smw5lgvd7ma` (`loser_id`),
                               KEY `FKmt9gdqajf9r8ft6lrvtb6xpni` (`winner_id`),
                               CONSTRAINT `FKmt9gdqajf9r8ft6lrvtb6xpni` FOREIGN KEY (`winner_id`) REFERENCES `warrior` (`id`),
                               CONSTRAINT `FKrd50k8m9ppvm01smw5lgvd7ma` FOREIGN KEY (`loser_id`) REFERENCES `warrior` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;