package com.game.LOTF.service;

import com.game.LOTF.dto.InventoryDTO;
import com.game.LOTF.entity.Inventory;
import com.game.LOTF.entity.Item;

public interface InventoryService {

    Inventory getInventory(Long characterId);

    InventoryDTO getInventoryDTO(Long characterId);

    void addItem(Item item, Long characterId);

    void removeItem(Item item, Long characterId);

}
