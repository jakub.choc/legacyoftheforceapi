package com.game.LOTF.service;

import java.util.List;
import com.game.LOTF.dto.FightRecapDTO;
import com.game.LOTF.entity.Character;

public interface FightRecapService {

    FightRecapDTO create(Character character, Character enemy, int characterHealth);

    List<FightRecapDTO> history(Character character);
}
