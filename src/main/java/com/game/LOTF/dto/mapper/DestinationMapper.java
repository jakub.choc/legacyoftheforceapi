package com.game.LOTF.dto.mapper;

import com.game.LOTF.dto.DestinationDTO;
import com.game.LOTF.entity.Destination;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface DestinationMapper {

    @Mapping(target = "characters", ignore = true)
    DestinationDTO toDTO(Destination destination);
}
