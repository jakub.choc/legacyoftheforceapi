package com.game.LOTF.entity;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import jakarta.persistence.OneToOne;
import lombok.Getter;
import lombok.Setter;
import java.util.List;
import java.util.Random;
import com.game.LOTF.en.CharacterStatus;

@Entity (name = "warrior")
@Setter
@Getter
public class Character {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private int health;
    private int maxHealth;
    private int stamina;
    private int maxStamina;
    private int experience;
    private int maxExperience;
    private int damage;
    private int armour;
    private int level;
    private int score;
    private int money;
    @ManyToOne
    private Race race;
    private String profilePhoto;
    @OneToOne
    @JoinColumn(name = "inventory_id")
    private Inventory inventory;
    @OneToMany(mappedBy = "winner")
    private List<FightRecap> wonFights;
    @OneToMany(mappedBy = "loser")
    private List<FightRecap> lostFights;
    @ManyToOne
    @JoinColumn(name = "destination_id")
    private Destination currentDestination;
    private CharacterStatus status;

    public boolean isAlive() {
        return health > 0;
    }

    public String getGraphicHealth() {
        if (health < 0) {
            return "Character is dead";
        }
        var percent = ((double) 100 / this.maxHealth) * health;
        var average = percentAverage(percent);
        StringBuilder graph = new StringBuilder("[");

        for (int i = 1; i <= 10; i++) {
            graph.append(i <= average ? "#" : " ");
        }
        graph.append("] ").append(health).append("/").append(maxHealth).append(" (").append(percent).append("%)");

        return graph.toString();
    }

    public String getGraphicStamina() {
        if (stamina < 0) {
            return "Not enough stamina";
        }
        double percent = (stamina * 100.0) / maxStamina;
        int average = (int) (percent / 10.0);
        StringBuilder graph = new StringBuilder("[");

        for (int i = 1; i <= 10; i++) {
            graph.append(i <= average ? "#" : " ");
        }
        graph.append("] ").append(stamina).append("/").append(maxStamina).append(" (").append(percent).append("%)");

        return graph.toString();
    }

    public String getGraphicExperience() {
        var percent = ((double) 100 / maxExperience) * experience;
        var average = percentAverage(percent);
        StringBuilder graph = new StringBuilder("[");

        for (int i = 1; i <= 10; i++) {
            graph.append(i <= average ? "#" : " ");
        }
        graph.append("} ").append(experience).append("/").append(maxStamina).append("(").append(percent).append("&");

        return graph.toString();
    }

    private int percentAverage(double number) {
        int closestValue = (int) (number / 10) * 10;

        if (number - closestValue < 5) {
            return closestValue;
        } else {
            return closestValue + 10;
        }
    }

    public void attack(Character enemy) {
        if (stamina > 0) {
            enemy.defend(countCritical(damage));
            stamina -= 1;
        }
    }

    public void defend(int enemyDamage) {
        var injury = calculateInjury(enemyDamage, armour);
        if (injury > 0) {
            health -= injury;
            if(health <= 0) {
                health = 0;
            }
        }
    }

    private int calculateInjury(int damage, int armour) {
        Double percentOfDamageReduce = ((double) 100 / (100 + armour)) * 100;
        var finalDamage = percentOfDamageReduce * ((double)damage / 100);
        return (int)finalDamage;
    }

    public int countCritical(int damage) {
        double probability = damage / 100.0;
        Random random = new Random();
        double randomNumber = random.nextDouble();

        if (randomNumber < probability) {
            return (int) (damage * 1.75);
        }
        else {
            return damage;
        }
    }

    public void addExperience(int newXP) {
        if (maxExperience < experience + newXP) {
            setLevel(level + 1);
            setExperience((newXP - experience));
        } else {
            setExperience(experience + newXP);
        }
    }

    public void minusScore() {
        if (score < 2) {
            setScore(0);
        } else {
            setScore(score - 2);
        }
    }

    public void plusScore() {
        setScore(score + 2);
    }

    public void addMoney(int add) {
        setMoney(money + add);
    }

    public void takeMoney(int take) {
        setMoney(money - take);
    }

}
