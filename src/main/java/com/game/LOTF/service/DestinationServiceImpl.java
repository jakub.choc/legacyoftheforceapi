package com.game.LOTF.service;

import jakarta.persistence.EntityNotFoundException;
import lombok.AllArgsConstructor;
import java.util.List;
import java.util.stream.Collectors;
import com.game.LOTF.dto.CharacterDTO;
import com.game.LOTF.dto.DestinationDTO;
import com.game.LOTF.dto.mapper.CharacterMapper;
import com.game.LOTF.dto.mapper.DestinationMapper;
import com.game.LOTF.entity.Character;
import com.game.LOTF.entity.Destination;
import com.game.LOTF.entity.repository.DestinationRepository;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class DestinationServiceImpl implements DestinationService {

    private final DestinationRepository destinationRepository;
    private final DestinationMapper destinationMapper;
    private final CharacterMapper characterMapper;

    @Override
    public DestinationDTO getDestination(Long destinationId) {
        Destination destination = destinationRepository.findById(destinationId)
                .orElseThrow(EntityNotFoundException::new);
        List<Character> characters = destination.getCharacters();
        List<CharacterDTO> characterDTOs = characters.stream()
                .map(characterMapper::toDTOBasics)
                .collect(Collectors.toList());
        DestinationDTO destinationDTO = destinationMapper.toDTO(destination);
        destinationDTO.setCharacters(characterDTOs);
        return destinationDTO;
    }

    @Override
    public Destination getDestinationEntity(Long destinationId) {
        return destinationRepository.findById(destinationId)
                .orElseThrow(EntityNotFoundException::new);
    }

    @Override
    public List<DestinationDTO> getAllDestinations() {
        return destinationRepository.findAll().stream()
                .map(destination -> {
                    List<CharacterDTO> characterDTOs = destination.getCharacters()
                            .stream()
                            .map(characterMapper::toDTOBasics)
                            .collect(Collectors.toList());

                    DestinationDTO destinationDTO = destinationMapper.toDTO(destination);
                    destinationDTO.setCharacters(characterDTOs);
                    return destinationDTO;
                })
                .collect(Collectors.toList());
    }
}
