package com.game.LOTF.entity;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import lombok.Getter;
import lombok.Setter;
import java.time.ZonedDateTime;
import com.game.LOTF.en.TravelStatus;

@Entity(name = "travel_record")
@Setter
@Getter
public class TravelRecord {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @ManyToOne
    @JoinColumn(name = "character_id")
    private Character character;
    @ManyToOne
    @JoinColumn(name = "destination_id")
    private Destination startDestination;
    @ManyToOne
    @JoinColumn(name = "destination_id")
    private Destination finalDestination;
    private ZonedDateTime end;
    private TravelStatus status;

}
