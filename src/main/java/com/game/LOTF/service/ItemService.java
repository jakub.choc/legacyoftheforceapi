package com.game.LOTF.service;

import com.game.LOTF.entity.Inventory;
import com.game.LOTF.entity.Item;

import java.util.List;

public interface ItemService {

    List<Item> getItemsByInventory(Inventory inventory);

    Item getItem(Long itemId);

    void saveItem(Item item);

}
