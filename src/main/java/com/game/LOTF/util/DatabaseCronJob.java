package com.game.LOTF.util;

import lombok.AllArgsConstructor;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import com.game.LOTF.service.TravelService;

@AllArgsConstructor
public class DatabaseCronJob {

    private final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);
    private final TravelService travelService;

    public void startCronJob() {
        scheduler.scheduleAtFixedRate(this::checkDatabase, 0, 1, TimeUnit.MINUTES);
    }

    private void checkDatabase() {
        System.out.println("Cron job running...");
        travelService.checkTravelRecords();
    }

}
