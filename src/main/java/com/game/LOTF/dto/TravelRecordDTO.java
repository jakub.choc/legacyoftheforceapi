package com.game.LOTF.dto;

import lombok.Data;
import java.time.ZonedDateTime;
import com.fasterxml.jackson.annotation.JsonProperty;

@Data
public class TravelRecordDTO {

    @JsonProperty("_id")
    private Long id;
    private String startDestination;
    private String finalDestination;
    private ZonedDateTime end;

}
