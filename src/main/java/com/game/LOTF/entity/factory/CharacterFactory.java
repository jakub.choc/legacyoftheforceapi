package com.game.LOTF.entity.factory;

import com.game.LOTF.entity.Character;
import com.game.LOTF.entity.Inventory;
import com.game.LOTF.entity.Race;
import org.springframework.stereotype.Service;

@Service
public class CharacterFactory {
    public Character createCharacter(String name, Race race) {
        Character entity = new Character();
        entity.setName(name);
        entity.setRace(race);
        entity.setHealth(100);
        entity.setMaxHealth(100);
        entity.setStamina(5);
        entity.setMaxStamina(5);
        entity.setExperience(0);
        entity.setMaxStamina(10);
        entity.setDamage(1);
        entity.setArmour(1);
        entity.setProfilePhoto("");
        return entity;
    }

    public Inventory createInventory(Character character) {
        Inventory inventory = new Inventory();
        inventory.setCharacter(character);
        return inventory;
    }

}
