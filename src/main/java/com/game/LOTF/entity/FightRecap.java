package com.game.LOTF.entity;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import lombok.Getter;
import lombok.Setter;
import java.time.LocalDateTime;

@Entity(name = "fight_recap")
@Getter
@Setter
public class FightRecap {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @ManyToOne
    @JoinColumn(name = "winner_id")
    private Character winner;
    @ManyToOne
    @JoinColumn(name = "loser_id")
    private Character loser;
    private LocalDateTime fightTime;
}
