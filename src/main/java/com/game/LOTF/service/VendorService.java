package com.game.LOTF.service;

import com.game.LOTF.dto.CharacterDTO;
import com.game.LOTF.dto.VendorDTO;

public interface VendorService {

    CharacterDTO sellItem(Long characterId, Long itemId, Long vendorId);

    CharacterDTO buyItem(Long characterId, Long itemId, Long vendorId);

    VendorDTO getVendor(Long vendorId);
}
