package com.game.LOTF.service;

import java.util.List;
import com.game.LOTF.dto.CharacterDTO;
import com.game.LOTF.dto.FightRecapDTO;
import com.game.LOTF.dto.InventoryDTO;
import com.game.LOTF.entity.Character;


public interface CharacterService {

    CharacterDTO getCharacter(Long characterId);

    Character getCharacterEntity(Long characterId);

    CharacterDTO createCharacter(CharacterDTO characterDTO);

    CharacterDTO editCharacter(CharacterDTO characterDTO);

    InventoryDTO getInventory(Long characterId);

    CharacterDTO addExperience(Long characterId, int newXP);

    FightRecapDTO doFight(Long characterId, Long enemyId);

    List<FightRecapDTO> getFightHistory(Long characterId);

    List<CharacterDTO> getAllCharacters();

    void saveCharacter(Character character);

}
