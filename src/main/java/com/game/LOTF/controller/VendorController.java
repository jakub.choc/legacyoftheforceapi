package com.game.LOTF.controller;

import lombok.AllArgsConstructor;
import com.game.LOTF.dto.CharacterDTO;
import com.game.LOTF.dto.VendorDTO;
import com.game.LOTF.service.VendorService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@AllArgsConstructor
@RequestMapping("/api")
public class VendorController {

    private final VendorService vendorService;

    @GetMapping("/vendor/vendorId")
    public VendorDTO getVendorItems(@RequestParam Long vendorId) {
        return vendorService.getVendor(vendorId);
    }

    @PostMapping("/vendor/sell")
    public CharacterDTO sellItem(@RequestParam Long vendorId, @RequestParam Long characterId, @RequestParam Long itemId) {
        return vendorService.sellItem(characterId, itemId, vendorId);
    }

    @PostMapping("/vendor/buy")
    public CharacterDTO buyItem(@RequestParam Long vendorId, @RequestParam Long characterId, @RequestParam Long itemId) {
        return vendorService.buyItem(characterId, itemId, vendorId);
    }
}
