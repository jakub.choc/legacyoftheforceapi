package com.game.LOTF.dto.mapper;

import com.game.LOTF.dto.InventoryDTO;
import com.game.LOTF.entity.Character;
import com.game.LOTF.entity.Inventory;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    comments = "version: 1.5.3.Final, compiler: javac, environment: Java 17.0.7 (Amazon.com Inc.)"
)
@Component
public class InventoryMapperImpl implements InventoryMapper {

    @Override
    public InventoryDTO toDTO(Inventory source) {
        if ( source == null ) {
            return null;
        }

        InventoryDTO inventoryDTO = new InventoryDTO();

        inventoryDTO.setCharacter( sourceCharacterId( source ) );
        inventoryDTO.setId( source.getId() );

        inventoryDTO.setItems( getItemIds(source) );

        return inventoryDTO;
    }

    private Long sourceCharacterId(Inventory inventory) {
        if ( inventory == null ) {
            return null;
        }
        Character character = inventory.getCharacter();
        if ( character == null ) {
            return null;
        }
        Long id = character.getId();
        if ( id == null ) {
            return null;
        }
        return id;
    }
}
