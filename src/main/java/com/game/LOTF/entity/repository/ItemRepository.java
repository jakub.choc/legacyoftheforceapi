package com.game.LOTF.entity.repository;

import com.game.LOTF.entity.Item;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;


public interface ItemRepository extends JpaRepository<Item, Long> {

    @Query(
            value = """
            SELECT * FROM item WHERE inventory_id = :id
            """,
            nativeQuery = true)
    List<Item> findByInventoryId(@Param("id") Long id);

}
