package com.game.LOTF.service;

import lombok.AllArgsConstructor;
import java.time.ZonedDateTime;
import java.util.List;
import com.game.LOTF.dto.TravelRecordDTO;
import com.game.LOTF.dto.mapper.TravelMapper;
import com.game.LOTF.en.CharacterStatus;
import com.game.LOTF.en.TravelStatus;
import com.game.LOTF.entity.Character;
import com.game.LOTF.entity.Destination;
import com.game.LOTF.entity.TravelRecord;
import com.game.LOTF.entity.repository.TravelRepository;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class TravelServiceImpl implements TravelService {

    private static final Logger log = Logger.getLogger(TravelServiceImpl.class);
    private final TravelRepository travelRepository;
    private final TravelMapper travelMapper;
    private final DestinationService destinationService;
    private final CharacterService characterService;

    @Override
    public TravelRecordDTO travel(Long characterId, Long startDestinationId, Long finishDestinationId) {
        Character character = characterService.getCharacterEntity(characterId);
        character.setStatus(CharacterStatus.TRAVEL);
        TravelRecord newRecord = createRecord(character, startDestinationId, finishDestinationId);
        characterService.saveCharacter(character);
        return travelMapper.toDTO(newRecord);
    }

    private TravelRecord createRecord(Character character, Long startDestinationId, Long finishDestinationId) {
        Destination startDestination = destinationService.getDestinationEntity(startDestinationId);
        Destination finishedDestination = destinationService.getDestinationEntity(finishDestinationId);
        TravelRecord travelRecord = new TravelRecord();
        travelRecord.setStartDestination(startDestination);
        travelRecord.setFinalDestination(finishedDestination);
        travelRecord.setCharacter(character);
        travelRecord.setEnd(ZonedDateTime.now().plusMinutes(5));
        travelRecord.setStatus(TravelStatus.ACTIVE);
        travelRepository.save(travelRecord);
        log.info(String.format("Travel destination for character id: %d was created", character.getId()));
        return travelRecord;
    }

    @Override
    public void checkTravelRecords() {
        List<TravelRecord> activeTravels = travelRepository.getAllActiveTravels();
        for (TravelRecord record : activeTravels) {
            if (record.getEnd().isAfter(ZonedDateTime.now())) {
                editUserAfterTravel(record);
                record.setStatus(TravelStatus.NON_ACTIVE);
                travelRepository.save(record);
                log.info(String.format("Travel record id: %d is non-active", record.getId()));
            }
        }
    }

    public void editUserAfterTravel(TravelRecord record) {
        Character character = characterService.getCharacterEntity(record.getCharacter().getId());
        character.setStatus(CharacterStatus.DESTINATION);
        character.setCurrentDestination(record.getFinalDestination());
        characterService.saveCharacter(character);
        log.info(String.format("character id: %d arrived into destination id: %d", character.getId(),
                record.getFinalDestination().getId()));
    }

}
