package com.game.LOTF.dto.mapper;

import com.game.LOTF.dto.InventoryDTO;
import com.game.LOTF.entity.Inventory;
import com.game.LOTF.entity.Item;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.ArrayList;
import java.util.List;


@Mapper(componentModel = "spring")
public interface InventoryMapper {

    @Mapping(target = "items", expression = "java(getItemIds(source))")
    @Mapping(target = "character", source = "character.id")
    InventoryDTO toDTO(Inventory source);

    //Inventory toEntity(InventoryDTO inventoryDTO);

    default List<Long> getItemIds(Inventory source) {
        List<Long> result = new ArrayList<>();
        for (Item item : source.getItems()) {
            result.add(item.getId());
        }
        return result;
    }

}
