package com.game.LOTF.service;

import com.game.LOTF.entity.Inventory;
import com.game.LOTF.entity.Item;
import com.game.LOTF.entity.repository.ItemRepository;
import jakarta.persistence.EntityNotFoundException;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
@AllArgsConstructor
public class ItemServiceImpl implements ItemService {

    private final ItemRepository itemRepository;

    @Override
    public List<Item> getItemsByInventory(Inventory inventory) {
        return itemRepository.findByInventoryId(inventory.getId());
    }

    @Override
    public Item getItem(Long itemId) {
        return itemRepository.findById(itemId).orElseThrow(EntityNotFoundException::new);
    }

    @Override
    public void saveItem(Item item) {
        itemRepository.save(item);
    }

}
