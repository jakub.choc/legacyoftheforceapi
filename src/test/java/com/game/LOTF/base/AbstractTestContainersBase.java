package com.game.LOTF.base;

import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.testcontainers.containers.MySQLContainer;
import org.testcontainers.utility.DockerImageName;

@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("integration-test")
@AutoConfigureMockMvc
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public abstract class AbstractTestContainersBase {

    private static final MySQLContainer mysqlLegacy;

    static {
        mysqlLegacy = new MySQLContainer<>(DockerImageName.parse("mysql:8.0"))
                .withDatabaseName("gamedatabase")
                .withInitScript("db/legacy_schema.sql")
                .withUsername("root")
                .withPassword("user")
                .withReuse(true)
                .withLabel("appname", "legacyoftheforceapi");
        mysqlLegacy.start();
    }

    @DynamicPropertySource
    static void properties(DynamicPropertyRegistry registry) {
        registry.add("embedded.mysql.url", mysqlLegacy::getJdbcUrl);
        registry.add("embedded.mysql.user", mysqlLegacy::getUsername);
        registry.add("embedded.mysql.password", mysqlLegacy::getPassword);
    }
}
