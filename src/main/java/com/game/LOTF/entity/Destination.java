package com.game.LOTF.entity;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import lombok.Getter;
import lombok.Setter;
import java.util.List;

@Entity(name = "destination")
@Setter
@Getter
public class Destination {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    @OneToMany(mappedBy = "currentDestination")
    private List<Character> characters;
    private int minimumLevel;
    private String destinationPhoto;
    @OneToMany(mappedBy = "startDestination")
    private List<TravelRecord> starts;
    @OneToMany(mappedBy = "finalDestination")
    private List<TravelRecord> ends;

}
