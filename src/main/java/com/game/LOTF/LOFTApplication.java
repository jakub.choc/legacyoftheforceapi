package com.game.LOTF;

import com.game.LOTF.dto.mapper.TravelMapper;
import com.game.LOTF.entity.repository.TravelRepository;
import com.game.LOTF.service.CharacterService;
import com.game.LOTF.service.DestinationService;
import com.game.LOTF.service.TravelService;
import com.game.LOTF.service.TravelServiceImpl;
import com.game.LOTF.util.DatabaseCronJob;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;


@SpringBootApplication(exclude = { SecurityAutoConfiguration.class })
public class LOFTApplication {

    public static void main(String[] args) {
        SpringApplication.run(LOFTApplication.class, args);


        TravelRepository travelRepository = new Trave
        TravelMapper travelMapper;
        DestinationService destinationService;
        CharacterService characterService;
        TravelService travelService = new TravelServiceImpl();
        DatabaseCronJob cronJob = new DatabaseCronJob(travelService);
        cronJob.startCronJob();
    }

}
