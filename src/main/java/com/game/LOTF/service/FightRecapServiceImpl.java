package com.game.LOTF.service;

import lombok.AllArgsConstructor;
import java.time.LocalDateTime;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import com.game.LOTF.dto.FightRecapDTO;
import com.game.LOTF.dto.mapper.FightRecapMapper;
import com.game.LOTF.entity.Character;
import com.game.LOTF.entity.FightRecap;
import com.game.LOTF.entity.repository.FightRecapRepository;
import org.springframework.stereotype.Service;

@AllArgsConstructor
@Service
public class FightRecapServiceImpl implements FightRecapService {

    private final FightRecapRepository fightRecapRepository;
    private final FightRecapMapper fightRecapMapper;

    public FightRecapDTO create(Character character, Character enemy, int characterHealth) {
        FightRecap fightRecap = new FightRecap();
        if (characterHealth == 0) {
            fightRecap.setWinner(enemy);
            fightRecap.setLoser(character);
        } else {
            fightRecap.setWinner(character);
            fightRecap.setLoser(enemy);
        }
        fightRecap.setFightTime(LocalDateTime.now());
        FightRecap entity = fightRecapRepository.save(fightRecap);
        return fightRecapMapper.toDto(entity);
    }

    @Override
    public List<FightRecapDTO> history(Character character) {
        List<FightRecap> lost = character.getLostFights();
        List<FightRecap> won = character.getWonFights();
        lost.addAll(won);
        return lost.stream()
                .map(fightRecapMapper::toDto)
                .sorted(Comparator.comparing(FightRecapDTO::getFightTime))
                .collect(Collectors.toList());
    }

}
