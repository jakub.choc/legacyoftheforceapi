package com.game.LOTF.entity.repository;

import com.game.LOTF.entity.Race;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RaceRepository extends JpaRepository<Race, Long> {
}
