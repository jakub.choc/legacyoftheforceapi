package com.game.LOTF.dto.mapper;

import com.game.LOTF.dto.ItemDTO;
import com.game.LOTF.entity.Item;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    comments = "version: 1.5.3.Final, compiler: javac, environment: Java 17.0.7 (Amazon.com Inc.)"
)
@Component
public class ItemMapperImpl implements ItemMapper {

    @Override
    public ItemDTO toDTO(Item item) {
        if ( item == null ) {
            return null;
        }

        ItemDTO itemDTO = new ItemDTO();

        itemDTO.setId( item.getId() );
        itemDTO.setName( item.getName() );
        itemDTO.setIcon( item.getIcon() );
        itemDTO.setBuyPrice( item.getBuyPrice() );
        itemDTO.setSellPrice( item.getSellPrice() );

        return itemDTO;
    }
}
