package com.game.LOTF.dto.mapper;

import com.game.LOTF.dto.CharacterDTO;
import com.game.LOTF.entity.Character;
import com.game.LOTF.entity.Destination;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    comments = "version: 1.5.3.Final, compiler: javac, environment: Java 17.0.7 (Amazon.com Inc.)"
)
@Component
public class CharacterMapperImpl implements CharacterMapper {

    @Override
    public CharacterDTO toDTO(Character entity) {
        if ( entity == null ) {
            return null;
        }

        CharacterDTO characterDTO = new CharacterDTO();

        characterDTO.setDestination( entityCurrentDestinationName( entity ) );
        characterDTO.setId( entity.getId() );
        characterDTO.setName( entity.getName() );
        characterDTO.setHealth( entity.getHealth() );
        characterDTO.setMaxHealth( entity.getMaxHealth() );
        characterDTO.setStamina( entity.getStamina() );
        characterDTO.setMaxStamina( entity.getMaxStamina() );
        characterDTO.setExperience( entity.getExperience() );
        characterDTO.setMaxExperience( entity.getMaxExperience() );
        characterDTO.setDamage( entity.getDamage() );
        characterDTO.setArmour( entity.getArmour() );
        characterDTO.setLevel( entity.getLevel() );
        characterDTO.setScore( entity.getScore() );
        characterDTO.setMoney( entity.getMoney() );
        characterDTO.setRace( entity.getRace() );
        characterDTO.setProfilePhoto( entity.getProfilePhoto() );

        characterDTO.setGraphicHealth( entity.getGraphicHealth() );
        characterDTO.setGraphicExperience( entity.getGraphicExperience() );
        characterDTO.setGraphicStamina( entity.getGraphicStamina() );

        return characterDTO;
    }

    @Override
    public CharacterDTO toDTOBasics(Character entity) {
        if ( entity == null ) {
            return null;
        }

        CharacterDTO characterDTO = new CharacterDTO();

        characterDTO.setId( entity.getId() );
        characterDTO.setName( entity.getName() );
        characterDTO.setLevel( entity.getLevel() );
        characterDTO.setScore( entity.getScore() );
        characterDTO.setMoney( entity.getMoney() );
        characterDTO.setRace( entity.getRace() );
        characterDTO.setProfilePhoto( entity.getProfilePhoto() );

        return characterDTO;
    }

    @Override
    public Character toEntity(CharacterDTO dto) {
        if ( dto == null ) {
            return null;
        }

        Character character = new Character();

        character.setId( dto.getId() );
        character.setName( dto.getName() );

        return character;
    }

    private String entityCurrentDestinationName(Character character) {
        if ( character == null ) {
            return null;
        }
        Destination currentDestination = character.getCurrentDestination();
        if ( currentDestination == null ) {
            return null;
        }
        String name = currentDestination.getName();
        if ( name == null ) {
            return null;
        }
        return name;
    }
}
