package com.game.LOTF.dto.mapper;


import com.game.LOTF.dto.CharacterDTO;
import com.game.LOTF.entity.Character;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface CharacterMapper {

    @Mapping(target = "graphicHealth", expression = "java(entity.getGraphicHealth())")
    @Mapping(target = "graphicExperience", expression = "java(entity.getGraphicExperience())")
    @Mapping(target = "graphicStamina", expression = "java(entity.getGraphicStamina())")
    @Mapping(target = "inventory", ignore = true)
    @Mapping(target = "destination", source = "currentDestination.name")
    CharacterDTO toDTO(Character entity);

    @Mapping(target = "inventory", ignore = true)
    @Mapping(target = "maxHealth", ignore = true)
    @Mapping(target = "health", ignore = true)
    @Mapping(target = "stamina", ignore = true)
    @Mapping(target = "maxStamina", ignore = true)
    @Mapping(target = "experience", ignore = true)
    @Mapping(target = "maxExperience", ignore = true)
    @Mapping(target = "damage", ignore = true)
    @Mapping(target = "armour", ignore = true)
    @Mapping(target = "graphicHealth", ignore = true)
    @Mapping(target = "graphicExperience", ignore = true)
    @Mapping(target = "graphicStamina", ignore = true)
    CharacterDTO toDTOBasics(Character entity);

    @Mapping(target = "health", ignore = true)
    @Mapping(target = "maxHealth", ignore = true)
    @Mapping(target = "stamina", ignore = true)
    @Mapping(target = "maxStamina", ignore = true)
    @Mapping(target = "experience", ignore = true)
    @Mapping(target = "maxExperience", ignore = true)
    @Mapping(target = "damage", ignore = true)
    @Mapping(target = "armour", ignore = true)
    @Mapping(target = "level", ignore = true)
    @Mapping(target = "score", ignore = true)
    @Mapping(target = "money", ignore = true)
    @Mapping(target = "race", ignore = true)
    @Mapping(target = "profilePhoto", ignore = true)
    @Mapping(target = "inventory", ignore = true)
    @Mapping(target = "wonFights", ignore = true)
    @Mapping(target = "lostFights", ignore = true)
    Character toEntity(CharacterDTO dto);

}
