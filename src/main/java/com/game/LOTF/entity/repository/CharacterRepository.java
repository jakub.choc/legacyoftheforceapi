package com.game.LOTF.entity.repository;

import com.game.LOTF.entity.Character;
import org.springframework.data.jpa.repository.JpaRepository;


public interface CharacterRepository extends JpaRepository<Character, Long> {

}
