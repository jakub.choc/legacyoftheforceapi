package com.game.LOTF.dto;

import lombok.Data;
import java.time.LocalDateTime;
import com.fasterxml.jackson.annotation.JsonProperty;

@Data
public class FightRecapDTO {
    @JsonProperty("_id")
    private Long id;
    private String winner;
    private String loser;
    private LocalDateTime fightTime;
}
