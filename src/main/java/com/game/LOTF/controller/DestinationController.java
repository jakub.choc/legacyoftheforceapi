package com.game.LOTF.controller;

import lombok.AllArgsConstructor;
import java.util.List;
import com.game.LOTF.dto.DestinationDTO;
import com.game.LOTF.service.DestinationService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@AllArgsConstructor
@RequestMapping("/api")
public class DestinationController {

    private final DestinationService destinationService;

    @GetMapping("/destination")
    public DestinationDTO getDestination(@PathVariable Long destinationId) {
        return destinationService.getDestination(destinationId);
    }

    @GetMapping("/destinations")
    public List<DestinationDTO> getDestinations() {
        return destinationService.getAllDestinations();
    }
}
