package com.game.LOTF.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.game.LOTF.entity.Race;
import jakarta.validation.constraints.NotBlank;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@NoArgsConstructor
public class CharacterDTO {

    @JsonProperty("_id")
    private Long id;
    @NotBlank
    private String name;
    private int health;
    private int maxHealth;
    private int stamina;
    private int maxStamina;
    private int experience;
    private int maxExperience;
    private int damage;
    private int armour;
    private int level;
    private int score;
    private int money;
    private String graphicHealth;
    private String graphicExperience;
    private String graphicStamina;
    private Race race;
    private String profilePhoto;
    private InventoryDTO inventory;
    private String destination;
}
