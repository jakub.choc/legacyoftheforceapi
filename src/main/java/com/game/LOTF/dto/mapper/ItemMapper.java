package com.game.LOTF.dto.mapper;

import com.game.LOTF.dto.ItemDTO;
import com.game.LOTF.entity.Item;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;


@Mapper(componentModel = "spring")
public interface ItemMapper {

    @Mapping(target = "inventories", ignore = true)
    ItemDTO toDTO(Item item);

    //Item toEntity(ItemDTO itemDTO);
}
