package com.game.LOTF.base;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Assertions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.jdbc.datasource.init.ScriptUtils;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import java.nio.charset.StandardCharsets;
import java.sql.SQLException;
import javax.sql.DataSource;

public class AbstractControllerBase extends AbstractTestContainersBase {

    @Autowired
    protected MockMvc mockMvc;
    @Autowired
    protected ObjectMapper objectMapper;
    @Autowired
    private DataSource dataSource;


    protected ResultActions callPostRequest(String url, Object body) throws Exception {
        return callPostRequest(url, new LinkedMultiValueMap<>(), body);
    }

    protected ResultActions callPostRequest(String url, MultiValueMap<String, String> params, Object body) throws Exception {
        return mockMvc.perform(MockMvcRequestBuilders
                .post(url)
                .params(params)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(body)));
    }

    protected ResultActions callGetRequest(String url, MultiValueMap<String, String> params, Object body) throws Exception {
        return mockMvc.perform(MockMvcRequestBuilders
                .post(url)
                .params(params)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(body)));
    }

    protected void callPostRequestAndExpect200(String url, Object body) throws Exception {
        callPostRequestAndExpect200(url, new LinkedMultiValueMap<>(), body);
    }

    protected void callPostRequestAndExpect200(String url, MultiValueMap<String, String> params, Object body) throws Exception {
        var result = callPostRequest(url, params, body).andReturn();
        Assertions.assertEquals(HttpStatus.OK.value(), result.getResponse().getStatus());
    }

    public <T> T callGetRequestWithResponse(String url, MultiValueMap<String, String> params, Class<T> clazz) throws Exception {
        var result = mockMvc.perform(MockMvcRequestBuilders
                        .get(url)
                        .queryParams(params)
                        .contentType(MediaType.APPLICATION_JSON)
                )
                .andReturn()
                .getResponse();
        Assertions.assertEquals(HttpStatus.OK.value(), result.getStatus());

        return mapToObject(result, clazz);
    }

    public <T> T callPostRequestWithResponse(String url, MultiValueMap<String, String> params, Class<T> clazz) throws Exception {
        var result = mockMvc.perform(MockMvcRequestBuilders
                        .post(url)
                        .queryParams(params)
                        .contentType(MediaType.APPLICATION_JSON)
                )
                .andReturn()
                .getResponse();
        Assertions.assertEquals(HttpStatus.OK.value(), result.getStatus());

        return mapToObject(result, clazz);
    }

    private <T> T mapToObject(MockHttpServletResponse response, Class<T> typeRef) {
        try {
            return objectMapper.readValue(response.getContentAsString(), typeRef);
        } catch (Exception e) {
            Assertions.fail(e);
            return null;
        }
    }

    protected void executeSqlScript(String sqlScript) {
        try (var connection = dataSource.getConnection()) {
            ScriptUtils.executeSqlScript(connection, new ByteArrayResource(sqlScript.getBytes(StandardCharsets.UTF_8)));
        } catch (SQLException e) {
            Assertions.fail(e);
        }
    }

}
