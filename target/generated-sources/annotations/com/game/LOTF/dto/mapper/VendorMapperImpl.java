package com.game.LOTF.dto.mapper;

import com.game.LOTF.dto.CharacterDTO;
import com.game.LOTF.dto.VendorDTO;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    comments = "version: 1.5.3.Final, compiler: javac, environment: Java 17.0.7 (Amazon.com Inc.)"
)
@Component
public class VendorMapperImpl implements VendorMapper {

    @Override
    public VendorDTO fromCharacterToVendor(CharacterDTO characterDTO) {
        if ( characterDTO == null ) {
            return null;
        }

        VendorDTO vendorDTO = new VendorDTO();

        vendorDTO.setId( characterDTO.getId() );
        vendorDTO.setMoney( characterDTO.getMoney() );
        vendorDTO.setInventory( characterDTO.getInventory() );

        return vendorDTO;
    }
}
