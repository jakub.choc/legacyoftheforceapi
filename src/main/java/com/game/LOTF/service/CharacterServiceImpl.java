package com.game.LOTF.service;

import com.game.LOTF.dto.CharacterDTO;
import com.game.LOTF.dto.FightRecapDTO;
import com.game.LOTF.dto.InventoryDTO;
import com.game.LOTF.dto.mapper.CharacterMapper;
import com.game.LOTF.entity.Character;
import com.game.LOTF.entity.Inventory;
import com.game.LOTF.entity.factory.CharacterFactory;
import com.game.LOTF.entity.repository.CharacterRepository;
import jakarta.persistence.EntityNotFoundException;
import lombok.AllArgsConstructor;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.stereotype.Service;


@Service
@AllArgsConstructor
public class CharacterServiceImpl implements CharacterService {

    private final CharacterFactory characterFactory;
    private final CharacterMapper characterMapper;
    private final CharacterRepository characterRepository;
    private final InventoryService inventoryService;
    private final FightRecapService fightRecapService;

    @Override
    public CharacterDTO getCharacter(Long characterId) {
        Character entity = characterRepository.getReferenceById(characterId);
        InventoryDTO inventoryDTO = inventoryService.getInventoryDTO(characterId);
        CharacterDTO characterDTO = characterMapper.toDTO(entity);
        characterDTO.setInventory(inventoryDTO);
        return characterDTO;
    }

    @Override
    public Character getCharacterEntity(Long characterId) {
        Character character = characterRepository.findById(characterId).orElseThrow(EntityNotFoundException::new);
        character.setInventory(inventoryService.getInventory(characterId));
        return character;
    }

    public InventoryDTO getInventory(Long characterId) {
        return inventoryService.getInventoryDTO(characterId);
    }

    @Override
    public CharacterDTO addExperience(Long characterId, int newXP) {
        Character character = characterRepository.findById(characterId).orElseThrow(EntityNotFoundException::new);
        character.addExperience(newXP);
        characterRepository.save(character);
        return getCharacter(characterId);
    }

    @Override
    public FightRecapDTO doFight(Long characterId, Long enemyId) {
        Character character = characterRepository.findById(characterId).orElseThrow(EntityNotFoundException::new);
        Character enemy = characterRepository.findById(enemyId).orElseThrow(EntityNotFoundException::new);
        return characterFight(character, enemy);
    }

    @Override
    public List<FightRecapDTO> getFightHistory(Long characterId) {
        Character entity = characterRepository.findById(characterId).orElseThrow(EntityNotFoundException::new);
        return fightRecapService.history(entity);
    }

    @Override
    public List<CharacterDTO> getAllCharacters() {
        List<Character> characters = characterRepository.findAll();
        return characters.stream()
                .map(characterMapper::toDTOBasics)
                .sorted(Comparator.comparing(CharacterDTO::getScore).reversed())
                .collect(Collectors.toList());
    }

    @Override
    public void saveCharacter(Character character) {
        characterRepository.save(character);
    }

    @Override
    public CharacterDTO createCharacter(CharacterDTO characterDTO) {
        Character entity = characterFactory.createCharacter(characterDTO.getName(), characterDTO.getRace());
        Inventory inventory = characterFactory.createInventory(entity);
        entity.setInventory(inventory);
        characterRepository.save(entity);
        return characterMapper.toDTO(entity);
    }

    @Override
    public CharacterDTO editCharacter(CharacterDTO characterDTO) {
        Character entity = characterMapper.toEntity(characterDTO);
        characterRepository.save(entity);
        return characterMapper.toDTO(entity);
    }

    private FightRecapDTO characterFight(Character character, Character enemy) {
        int characterHealth = character.getHealth();
        int enemyHealth = enemy.getHealth();
        do {
            character.attack(enemy);
            enemy.attack(character);
        } while (characterHealth == 0 || enemyHealth == 0);
        setScore(character, enemy);
        return fightRecapService.create(character, enemy, characterHealth);
    }

    private void setScore(Character character, Character enemy) {
        if (character.getHealth() == 0) {
            enemy.plusScore();
            character.minusScore();
        } else {
            character.plusScore();
            enemy.minusScore();
        }
        characterRepository.save(character);
        characterRepository.save(enemy);
    }
}
