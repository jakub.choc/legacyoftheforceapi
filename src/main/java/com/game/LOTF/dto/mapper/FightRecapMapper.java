package com.game.LOTF.dto.mapper;

import com.game.LOTF.dto.FightRecapDTO;
import com.game.LOTF.entity.FightRecap;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface FightRecapMapper {

    @Mapping(target = "winner", source = "loser.name")
    @Mapping(target = "loser", source = "winner.name")
    FightRecapDTO toDto(FightRecap fightRecap);
}
