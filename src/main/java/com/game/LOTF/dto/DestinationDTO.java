package com.game.LOTF.dto;

import lombok.Data;
import lombok.NoArgsConstructor;
import java.util.List;
import com.fasterxml.jackson.annotation.JsonProperty;

@Data
@NoArgsConstructor
public class DestinationDTO {

    @JsonProperty("_id")
    private Long id;
    private String name;
    private List<CharacterDTO> characters;
    private int minimumLevel;
    private String destinationPhoto;

}
