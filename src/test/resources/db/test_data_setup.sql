INSERT INTO fight_recap(id, fight_time, loser_id, winner_id)
VALUES (1, '2023-01-01 11:11:11', 1, 2)
;

INSERT INTO warrior(id, name, health, max_health, stamina, max_stamina, experience, max_experience, damage, armour, level, score, money, profile_photo, race, inventory)
VALUES (1, 'Dajacob', 100, 100, 100, 100, 30, 50, 10, 10, 1, 0, 100, '000.png', 1, 1),
       (2, 'Voldemord', 100, 100, 100, 100, 30, 50, 10, 10, 1, 0, 100, '000.png', 1, 1)
;

INSERT INTO inventory(id,  character)
VALUES (1, 1),
       (2, 2)
;

INSERT INTO race(id,  name)
VALUES(1, 'human')
;

