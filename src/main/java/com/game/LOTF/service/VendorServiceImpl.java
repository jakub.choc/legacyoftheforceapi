package com.game.LOTF.service;

import jakarta.persistence.EntityNotFoundException;
import lombok.AllArgsConstructor;
import com.game.LOTF.dto.CharacterDTO;
import com.game.LOTF.dto.VendorDTO;
import com.game.LOTF.dto.mapper.VendorMapper;
import com.game.LOTF.entity.Character;
import com.game.LOTF.entity.Item;
import com.game.LOTF.entity.Vendor;
import com.game.LOTF.entity.repository.VendorRepository;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class VendorServiceImpl implements VendorService {

    private final CharacterService characterService;
    private final VendorRepository vendorRepository;
    private final InventoryService inventoryService;
    private final ItemService itemService;
    private final VendorMapper vendorMapper;

    @Override
    public CharacterDTO sellItem(Long characterId, Long itemId, Long vendorId) {
        Vendor vendor = vendorRepository.findById(vendorId).orElseThrow(EntityNotFoundException::new);
        Item item = itemService.getItem(itemId);
        if (vendor.getMoney() < item.getSellPrice()) {
            throw new IllegalArgumentException("Vendor doesn't enough money");
        }
        inventoryService.addItem(item, vendorId);
        inventoryService.removeItem(item, characterId);

        Character character = characterService.getCharacterEntity(characterId);
        character.addMoney(item.getSellPrice());
        vendor.takeMoney(item.getSellPrice());
        characterService.saveCharacter(character);
        vendorRepository.save(vendor);

        return characterService.getCharacter(characterId);
    }

    @Override
    public CharacterDTO buyItem(Long characterId, Long itemId, Long vendorId) {
        Vendor vendor = vendorRepository.findById(vendorId).orElseThrow(EntityNotFoundException::new);
        Character character = characterService.getCharacterEntity(characterId);
        Item item = itemService.getItem(itemId);
        if (character.getMoney() < item.getBuyPrice()) {
            throw new IllegalArgumentException("You dont have enough money");
        }
        inventoryService.addItem(item, characterId);
        inventoryService.removeItem(item, vendorId);

        character.takeMoney(item.getBuyPrice());
        vendor.addMoney(item.getBuyPrice());
        characterService.saveCharacter(character);
        vendorRepository.save(vendor);

        return characterService.getCharacter(characterId);
    }

    @Override
    public VendorDTO getVendor(Long vendorId) {
       CharacterDTO characterDTO = characterService.getCharacter(vendorId);
       return vendorMapper.fromCharacterToVendor(characterDTO);
    }

}
