package com.game.LOTF.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

@Data
public class ItemDTO {

    @JsonProperty("_id")
    private Long id;
    private String name;
    private String icon;
    private int buyPrice;
    private int sellPrice;
    private List<InventoryDTO> inventories;

}
