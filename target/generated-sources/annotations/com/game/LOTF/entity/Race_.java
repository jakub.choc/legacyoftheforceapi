package com.game.LOTF.entity;

import jakarta.persistence.metamodel.ListAttribute;
import jakarta.persistence.metamodel.SingularAttribute;
import jakarta.persistence.metamodel.StaticMetamodel;
import javax.annotation.processing.Generated;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Race.class)
public abstract class Race_ {

	public static volatile ListAttribute<Race, Character> entities;
	public static volatile SingularAttribute<Race, String> raceName;
	public static volatile SingularAttribute<Race, Long> id;

	public static final String ENTITIES = "entities";
	public static final String RACE_NAME = "raceName";
	public static final String ID = "id";

}

