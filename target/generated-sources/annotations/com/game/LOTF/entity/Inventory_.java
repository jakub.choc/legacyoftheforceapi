package com.game.LOTF.entity;

import jakarta.persistence.metamodel.ListAttribute;
import jakarta.persistence.metamodel.SingularAttribute;
import jakarta.persistence.metamodel.StaticMetamodel;
import javax.annotation.processing.Generated;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Inventory.class)
public abstract class Inventory_ {

	public static volatile SingularAttribute<Inventory, Character> character;
	public static volatile SingularAttribute<Inventory, Long> id;
	public static volatile ListAttribute<Inventory, Item> items;

	public static final String CHARACTER = "character";
	public static final String ID = "id";
	public static final String ITEMS = "items";

}

