package com.game.LOTF.dto;

import lombok.Data;
import com.fasterxml.jackson.annotation.JsonProperty;

@Data
public class VendorDTO {
    @JsonProperty("_id")
    private Long id;
    private int money;
    private InventoryDTO inventory;
}
