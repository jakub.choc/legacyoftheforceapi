package com.game.LOTF.entity;

import jakarta.persistence.metamodel.ListAttribute;
import jakarta.persistence.metamodel.SingularAttribute;
import jakarta.persistence.metamodel.StaticMetamodel;
import javax.annotation.processing.Generated;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Item.class)
public abstract class Item_ {

	public static volatile SingularAttribute<Item, Integer> buyPrice;
	public static volatile ListAttribute<Item, Inventory> inventories;
	public static volatile SingularAttribute<Item, String> name;
	public static volatile SingularAttribute<Item, String> icon;
	public static volatile SingularAttribute<Item, Long> id;

	public static final String BUY_PRICE = "buyPrice";
	public static final String INVENTORIES = "inventories";
	public static final String NAME = "name";
	public static final String ICON = "icon";
	public static final String ID = "id";

}

