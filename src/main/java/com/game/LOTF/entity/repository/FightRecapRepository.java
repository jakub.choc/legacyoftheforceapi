package com.game.LOTF.entity.repository;

import com.game.LOTF.entity.FightRecap;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FightRecapRepository extends JpaRepository<FightRecap, Long> {
}
