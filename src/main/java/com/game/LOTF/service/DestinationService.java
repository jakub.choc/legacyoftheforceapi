package com.game.LOTF.service;

import java.util.List;
import com.game.LOTF.dto.DestinationDTO;
import com.game.LOTF.entity.Destination;

public interface DestinationService {

    DestinationDTO getDestination(Long destinationId);

    Destination getDestinationEntity(Long destinationId);

    List<DestinationDTO> getAllDestinations();
}
