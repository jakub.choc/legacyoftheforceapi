package com.game.LOTF.en;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public enum CharacterStatus {
    TRAVEL("Travel"),
    DUNGEON("Dungeon"),
    FIGHT("Fight"),
    DESTINATION("Destination");

    public final String name;

}
