package com.game.LOTF.service;

import com.game.LOTF.dto.TravelRecordDTO;

public interface TravelService {

    TravelRecordDTO travel(Long characterId, Long startDestinationId, Long finishDestinationId);

    void checkTravelRecords();

}
