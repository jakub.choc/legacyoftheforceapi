package com.game.LOTF.controller;

import com.game.LOTF.dto.CharacterDTO;
import com.game.LOTF.dto.FightRecapDTO;
import com.game.LOTF.dto.InventoryDTO;
import com.game.LOTF.service.CharacterService;
import lombok.AllArgsConstructor;
import java.util.List;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


@RestController
@AllArgsConstructor
@RequestMapping("/api")
public class CharacterController {

    private final CharacterService characterService;

    @GetMapping("/character/characterId")
    public CharacterDTO getCharacter(@PathVariable Long characterId) {
        return characterService.getCharacter(characterId);
    }

    @PostMapping("/character/characterId")
    public CharacterDTO createCharacter(@RequestBody CharacterDTO characterDTO) {
        return characterService.createCharacter(characterDTO);
    }

    @PutMapping("/character/characterId")
    public CharacterDTO editCharacter(@RequestBody CharacterDTO characterDTO) {
        return characterService.editCharacter(characterDTO);
    }

    @GetMapping("/inventory/characterId")
    public InventoryDTO getInventory(@RequestParam Long characterId) {
        return characterService.getInventory(characterId);
    }

    @PostMapping("/fight/{characterId}/enemyId")
    public FightRecapDTO getFightHistory(@RequestParam Long characterId, @RequestParam Long enemyId) {
        return characterService.doFight(characterId, enemyId);
    }

    @GetMapping("/fight_history/characterId")
    public List<FightRecapDTO> getFightHistory(@RequestParam Long characterId) {
        return characterService.getFightHistory(characterId);
    }

    @GetMapping("/players")
    public List<CharacterDTO> getAllPlayers() {
        return characterService.getAllCharacters();
    }

}
