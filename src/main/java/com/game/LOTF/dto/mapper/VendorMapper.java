package com.game.LOTF.dto.mapper;

import com.game.LOTF.dto.CharacterDTO;
import com.game.LOTF.dto.VendorDTO;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface VendorMapper {

    VendorDTO fromCharacterToVendor(CharacterDTO characterDTO);
}
