package com.game.LOTF.dto.mapper;

import com.game.LOTF.dto.TravelRecordDTO;
import com.game.LOTF.entity.TravelRecord;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface TravelMapper {

    @Mapping(target = "startDestination", source = "startDestination.name")
    @Mapping(target = "finalDestination", source = "startDestination.name")
    TravelRecordDTO toDTO(TravelRecord travelRecord);

}
