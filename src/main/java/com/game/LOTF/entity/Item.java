package com.game.LOTF.entity;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToMany;
import lombok.Getter;
import lombok.Setter;

import java.util.List;


@Entity(name = "item")
@Setter
@Getter
public class Item {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    private String icon;

    @ManyToMany(mappedBy = "items")
    private List<Inventory> inventories;

    private int buyPrice;

    public int getSellPrice() {
        double percentValue = (buyPrice / 100) * 75;
        return (int)percentValue;
    }

}
