package com.game.LOTF.service;

import com.game.LOTF.dto.InventoryDTO;
import com.game.LOTF.dto.mapper.InventoryMapper;
import com.game.LOTF.entity.Inventory;
import com.game.LOTF.entity.Item;
import com.game.LOTF.entity.repository.InventoryRepository;
import jakarta.persistence.EntityNotFoundException;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
@AllArgsConstructor
public class InventoryServiceImpl implements InventoryService {

    private final InventoryRepository inventoryRepository;
    private final InventoryMapper inventoryMapper;
    private final ItemService itemService;

    @Override
    public Inventory getInventory(Long characterId) {
        Inventory inventory = inventoryRepository.findById(characterId)
                .orElseThrow(EntityNotFoundException::new);
        List<Item> items = itemService.getItemsByInventory(inventory);
        inventory.setItems(items);
        return inventory;
    }

    @Override
    public InventoryDTO getInventoryDTO(Long characterId) {
        Inventory inventory = inventoryRepository.findById(characterId)
                .orElseThrow(EntityNotFoundException::new);
        List<Item> items = itemService.getItemsByInventory(inventory);
        inventory.setItems(items);
        return inventoryMapper.toDTO(inventory);
    }

    @Override
    public void addItem(Item item, Long characterId) {
        Inventory inventory = getInventory(characterId);
        List<Inventory> itemInventories = item.getInventories();
        itemInventories.add(inventory);
        itemService.saveItem(item);
    }

    @Override
    public void removeItem(Item item, Long characterId) {
        Inventory inventory = getInventory(characterId);
        List<Inventory> itemInventories = item.getInventories();
        itemInventories.remove(inventory);
        itemService.saveItem(item);
    }
}
