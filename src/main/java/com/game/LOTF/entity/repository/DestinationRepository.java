package com.game.LOTF.entity.repository;

import com.game.LOTF.entity.Destination;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DestinationRepository extends JpaRepository<Destination, Long> {
}
