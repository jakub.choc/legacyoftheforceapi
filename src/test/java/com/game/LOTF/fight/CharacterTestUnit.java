package com.game.LOTF.fight;

import com.game.LOTF.entity.Character;
import com.game.LOTF.entity.Race;
import com.game.LOTF.entity.factory.CharacterFactory;
import org.junit.Assert;
import org.junit.Test;

public class CharacterTestUnit {

    @Test
    void whenCalledSsAliveShouldReturnTrue() {
        CharacterFactory characterFactory = new CharacterFactory();
        Character character = characterFactory.createCharacter("Someone", new Race());
        Assert.assertEquals(character.isAlive(), true);
    }

    @Test
    void whenCalledSsAliveShouldReturnFalse() {
        CharacterFactory characterFactory = new CharacterFactory();
        Character character = characterFactory.createCharacter("Someone", new Race());
        character.setHealth(0);
        Assert.assertEquals(character.isAlive(), false);
    }


}
