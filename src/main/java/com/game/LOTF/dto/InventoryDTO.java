package com.game.LOTF.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;


@Data
public class InventoryDTO {

    @JsonProperty("_id")
    private Long id;
    private Long character;
    private List<Long> items;

}
