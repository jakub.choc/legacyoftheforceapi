package com.game.LOTF.entity;

import com.game.LOTF.en.CharacterStatus;
import jakarta.persistence.metamodel.ListAttribute;
import jakarta.persistence.metamodel.SingularAttribute;
import jakarta.persistence.metamodel.StaticMetamodel;
import javax.annotation.processing.Generated;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Character.class)
public abstract class Character_ {

	public static volatile SingularAttribute<Character, Integer> damage;
	public static volatile SingularAttribute<Character, Race> race;
	public static volatile SingularAttribute<Character, Integer> level;
	public static volatile SingularAttribute<Character, Destination> currentDestination;
	public static volatile SingularAttribute<Character, Integer> stamina;
	public static volatile SingularAttribute<Character, Integer> health;
	public static volatile SingularAttribute<Character, Integer> maxStamina;
	public static volatile ListAttribute<Character, FightRecap> lostFights;
	public static volatile SingularAttribute<Character, Integer> maxExperience;
	public static volatile SingularAttribute<Character, Integer> experience;
	public static volatile SingularAttribute<Character, Inventory> inventory;
	public static volatile SingularAttribute<Character, Integer> armour;
	public static volatile SingularAttribute<Character, Integer> score;
	public static volatile SingularAttribute<Character, String> profilePhoto;
	public static volatile ListAttribute<Character, FightRecap> wonFights;
	public static volatile SingularAttribute<Character, Integer> money;
	public static volatile SingularAttribute<Character, String> name;
	public static volatile SingularAttribute<Character, Integer> maxHealth;
	public static volatile SingularAttribute<Character, Long> id;
	public static volatile SingularAttribute<Character, CharacterStatus> status;

	public static final String DAMAGE = "damage";
	public static final String RACE = "race";
	public static final String LEVEL = "level";
	public static final String CURRENT_DESTINATION = "currentDestination";
	public static final String STAMINA = "stamina";
	public static final String HEALTH = "health";
	public static final String MAX_STAMINA = "maxStamina";
	public static final String LOST_FIGHTS = "lostFights";
	public static final String MAX_EXPERIENCE = "maxExperience";
	public static final String EXPERIENCE = "experience";
	public static final String INVENTORY = "inventory";
	public static final String ARMOUR = "armour";
	public static final String SCORE = "score";
	public static final String PROFILE_PHOTO = "profilePhoto";
	public static final String WON_FIGHTS = "wonFights";
	public static final String MONEY = "money";
	public static final String NAME = "name";
	public static final String MAX_HEALTH = "maxHealth";
	public static final String ID = "id";
	public static final String STATUS = "status";

}

