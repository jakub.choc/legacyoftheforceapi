package com.game.LOTF.fight;

import com.game.LOTF.base.AbstractControllerBase;
import com.game.LOTF.dto.CharacterDTO;
import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.testcontainers.shaded.com.google.common.base.Charsets;
import org.testcontainers.shaded.com.google.common.io.Resources;

import java.io.IOException;
import java.util.List;

public class CharacterTestIntegration extends AbstractControllerBase {

    private static final String INIT_FILE = "db/test_data_setup.sql";


    @BeforeEach
    void setUp() {
        setUpData();
    }

    @Test
    void get_results() throws Exception {
        MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
        var checkTypesResponse = callGetRequestWithResponse(
                "/api/players",
                params,
                CharacterDTO.class
        );

        CharacterDTO dto = new CharacterDTO();
        dto.setId(1L);
        dto.setName("Dajacob");
        dto.setScore(10);

        Assert.assertEquals(List.of(dto), checkTypesResponse);
    }

    private void setUpData() {
        var url = Resources.getResource(INIT_FILE);
        try {
            var sqlScript = Resources.toString(url, Charsets.UTF_8);
            executeSqlScript(sqlScript);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
