package com.game.LOTF.entity;

import jakarta.persistence.metamodel.SingularAttribute;
import jakarta.persistence.metamodel.StaticMetamodel;
import java.time.LocalDateTime;
import javax.annotation.processing.Generated;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(FightRecap.class)
public abstract class FightRecap_ {

	public static volatile SingularAttribute<FightRecap, Character> winner;
	public static volatile SingularAttribute<FightRecap, Long> id;
	public static volatile SingularAttribute<FightRecap, Character> loser;
	public static volatile SingularAttribute<FightRecap, LocalDateTime> fightTime;

	public static final String WINNER = "winner";
	public static final String ID = "id";
	public static final String LOSER = "loser";
	public static final String FIGHT_TIME = "fightTime";

}

